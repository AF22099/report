import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class report  {
    private JButton TempuraButton;
    private JButton RamenBotton;
    private JButton UnazyuBotton;
    private JButton OyakodonBotton;
    private JButton KarageButton;
    private JButton UdonBotton;
    private JTextArea orderedItemsList;
    private JButton checkOutButton;
    private JLabel topLabel;
    private JPanel root;
    private JLabel GokeiLabel;

    void order(String Foodmenu) {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + Foodmenu + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation == 0) {
            JOptionPane.showMessageDialog(null, "Order for "+Foodmenu+" received");
            String  currentText = orderedItemsList.getText();
            orderedItemsList.setText((currentText+Foodmenu+"\n"));
        }
    }

    public report() {
        GokeiLabel.setText((String.valueOf(0)+"  yen"));
        final int[] money = {0};
        TempuraButton.setIcon(new ImageIcon(
                this.getClass().getResource("/Tempura.jpg")));
        TempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura");
                money[0] = money[0] +350;
                GokeiLabel.setText((String.valueOf(money[0])+"  yen"));
            }
        });
        KarageButton.setIcon(new ImageIcon(
                this.getClass().getResource("/Karage.jpg")));
        KarageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karage");
                money[0] = money[0] +170;
                GokeiLabel.setText((String.valueOf(money[0])+"  yen"));
            }
        });
        UnazyuBotton.setIcon(new ImageIcon(
                this.getClass().getResource("/Unazyu.jpg")));
        UnazyuBotton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Unazyu");
                money[0] = money[0] +760;
                GokeiLabel.setText((String.valueOf(money[0])+"  yen"));
            }
        });
        RamenBotton.setIcon(new ImageIcon(
                this.getClass().getResource("/Ramen.jpg")
        ));
        RamenBotton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen");
                money[0] = money[0] +550;
                GokeiLabel.setText((String.valueOf(money[0])+"  yen"));
            }
        });
        OyakodonBotton.setIcon(new ImageIcon(
                this.getClass().getResource("/Oyakodon.jpg")
        ));
        OyakodonBotton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Oyakodon");
                money[0] = money[0] +450;
                GokeiLabel.setText((String.valueOf(money[0])+"  yen"));
            }
        });
        UdonBotton.setIcon(new ImageIcon(
                this.getClass().getResource("/Udon.jpg")
        ));
        UdonBotton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon");
                money[0] = money[0] +320;
                GokeiLabel.setText((String.valueOf(money[0])+"  yen"));
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation2 = JOptionPane.showConfirmDialog(null,
                        "Would you like to Checkout?",
                        "Checkout",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation2 == 0) {
                    JOptionPane.showMessageDialog(null, "Thank you for Ordering!!");
                    orderedItemsList.setText("");
                    money[0]=0;
                    GokeiLabel.setText((String.valueOf(money[0])+"  yen"));
                }
            }
        });
    }
    public static void main(String[] args) {
        JFrame frame = new JFrame("report");
        frame.setContentPane(new report().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
